package kpi.fpm.kp.kp71;

import kpi.fpm.kp.kp71.dao.DAO;
import kpi.fpm.kp.kp71.dao.DAOImpl;

public class Main {
    public static void main(String[] args) {
        System.out.println("Lab 2");
        DAO dao = null;
        try {
            dao = DAO.getInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("Subject with id 1");
        System.out.println(dao.getSubject(1));
        System.out.println("Subject with id 23");
        System.out.println(dao.getSubject(23));
        System.out.println("Subject list");
        System.out.println(dao.getSubjectList());
    }
}
