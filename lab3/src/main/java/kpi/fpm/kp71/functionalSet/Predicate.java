package kpi.fpm.kp71.functionalSet;

public interface Predicate<T> {
   boolean invoke(T element);
}
