package kpi.fpm.kp.kp71.models;

import kpi.fpm.kp.kp71.annotations.Column;
import kpi.fpm.kp.kp71.annotations.Entity;
import kpi.fpm.kp.kp71.annotations.ForeignKey;
import kpi.fpm.kp.kp71.annotations.PrimaryKey;

import java.util.Date;
import java.util.Objects;

@Entity(table = "Marks")
public class Mark {
    @Column(name = "id")
    @PrimaryKey
    private long id;

    @Column(name = "mark")
    private int mark;

    @Column(name = "date")
    private Date date;

    @Column(name = "subject_id")
    @ForeignKey(tableRef = "Subjects", keyRef = "id")
    private long subjectId;

    @Column(name = "student_id")
    @ForeignKey(tableRef = "Students", keyRef = "id")
    private long studentId;

    @Column(name = "teacher_id")
    @ForeignKey(tableRef = "Teachers", keyRef = "id")
    private long teacherId;

    public Mark() {
    }

    public Mark(long id, int mark, Date date, long subjectId, long studentId, long teacherId) {
        this.id = id;
        this.mark = mark;
        this.date = date;
        this.subjectId = subjectId;
        this.studentId = studentId;
        this.teacherId = teacherId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public long getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(long subjectId) {
        this.subjectId = subjectId;
    }

    public long getStudentId() {
        return studentId;
    }

    public void setStudentId(long studentId) {
        this.studentId = studentId;
    }

    public long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(long teacherId) {
        this.teacherId = teacherId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Mark)) return false;
        Mark mark1 = (Mark) o;
        return getId() == mark1.getId() &&
                getMark() == mark1.getMark() &&
                getSubjectId() == mark1.getSubjectId() &&
                getStudentId() == mark1.getStudentId() &&
                getTeacherId() == mark1.getTeacherId() &&
                getDate().equals(mark1.getDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getMark(), getDate(), getSubjectId(), getStudentId(), getTeacherId());
    }

    @Override
    public String toString() {
        return "Mark{" +
                "id=" + id +
                ", mark=" + mark +
                ", date=" + date +
                ", subjectId=" + subjectId +
                ", studentId=" + studentId +
                ", teacherId=" + teacherId +
                '}';
    }
}
