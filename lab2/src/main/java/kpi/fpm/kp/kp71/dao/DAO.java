package kpi.fpm.kp.kp71.dao;

import kpi.fpm.kp.kp71.models.*;

import java.util.List;

public class DAO {
    //DAO for models
    private DAOImpl<Curator> curatorDAO;
    private DAOImpl<Group> groupDAO;
    private DAOImpl<Mark> markDAO;
    private DAOImpl<Student> studentDAO;
    private DAOImpl<Subject> subjectDAO;
    private DAOImpl<Teacher> teacherDAO;
    //instance
    private static DAO instance;

    private DAO() throws Exception {
        curatorDAO =  new DAOImpl<>(Curator.class);
        groupDAO = new DAOImpl<>(Group.class);
        markDAO =  new DAOImpl<>(Mark.class);
        studentDAO = new DAOImpl<>(Student.class);
        subjectDAO = new DAOImpl<>(Subject.class);
        teacherDAO = new DAOImpl<>(Teacher.class);
    }

    public static DAO getInstance() throws Exception {
        if (instance == null)
            instance = new DAO();
        return instance;
    }

    //curator
    public Curator getCurator(long id) {
        return curatorDAO .getEntity(id);
    }

    public List<Curator> getCuratorList() {
        return curatorDAO.getEntityList();
    }

    //group
    public Group getGroup(long id) {
        return groupDAO .getEntity(id);
    }

    public List<Group> getGroupList() {
        return groupDAO.getEntityList();
    }

    //mark
    public Mark getMark(long id) {
        return markDAO .getEntity(id);
    }

    public List<Mark> getMarkList() {
        return markDAO.getEntityList();
    }

    //student
    public Student getStudent(long id) {
        return studentDAO .getEntity(id);
    }

    public List<Student> getStudentList() {
        return studentDAO.getEntityList();
    }

    //subject
    public Subject getSubject(long id) {
        return subjectDAO.getEntity(id);
    }

    public List<Subject> getSubjectList() {
        return subjectDAO.getEntityList();
    }

    //teacher
    public Teacher getTeacher(long id) {
        return teacherDAO.getEntity(id);
    }

    public List<Teacher> getTeacherList() {
        return teacherDAO.getEntityList();
    }
}
