package kpi.fpm.kp71.fs;

import java.util.LinkedList;
import java.util.Queue;

public class BufferFile<T> extends FSNode {
    private final static int MAX_BUF_FILE_SIZE = 10;
    protected Queue<T> elements;

    private BufferFile(String name, Directory parent) {
        super(name, parent);
        elements = new LinkedList<>();
        this.fileType = FSNodeType.BUFFER_FILE;
    }

    public static <T> BufferFile<T> create(String name, Directory parent) {
        return new BufferFile<T>(name, parent);
    }

    public void push(T element) {
        readwrite(() -> {
            checkNull(element, "Could not add null element");
            if (elements.size() >= MAX_BUF_FILE_SIZE) {
                throw new IllegalStateException("Buffer overflow");
            }
            elements.add(element);
        });
    }

    public T consume() {
        return readwrite(() -> elements.remove());
    }
}
