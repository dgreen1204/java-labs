package kpi.fpm.kp.kp71;

import kpi.fpm.kp.kp71.dao.DAOImpl;
import kpi.fpm.kp.kp71.models.Teacher;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@DisplayName("Teacher")
@ExtendWith(MockitoExtension.class)
class TeacherDAOImplTest extends DAOImplTest {
    @Test
    @DisplayName("getEntityById")
    void testGetTeacherById() throws SQLException {
        DAOImpl<Teacher> teacherDAO = new DAOImpl<>(Teacher.class, mockConnection);
        when(mockResultSet.next()).thenReturn(true, false);
        Teacher teacherId1 = new Teacher(1, "Ruslan", "Hadyniak", 5, 1);
        when(mockResultSet.getObject(anyString())).thenAnswer(new Answer() {
            public Object answer(InvocationOnMock invocation) {
                switch ((String) invocation.getArguments()[0]) {
                    case "id":              return teacherId1.getId();
                    case "firstName":       return teacherId1.getFirstName();
                    case "lastName":        return teacherId1.getLastName();
                    case "experience":      return teacherId1.getExperience();
                    case "mainSubjectId":   return teacherId1.getMainSubjectId();
                }
                return null;
            }
        });
        Teacher t = teacherDAO.getEntity(1);
        System.out.println(t);
        assertEquals(teacherId1,t );
        assertNull(teacherDAO.getEntity(15));
    }

    @Test
    @DisplayName("getEntityList")
    void testGetTeacherList() throws SQLException {
        DAOImpl<Teacher> teacherDAO = new DAOImpl<>(Teacher.class, mockConnection);
        when(mockResultSet.next()).thenReturn(false,true, true, true, false);
        assertEquals(teacherDAO.getEntityList().size(), 0);
        long[] ids = new long[] {1L, 2L, 3L};
        String[] firstNames = new String[] { "Ruslan", "Viktor", "Volodymyr"};
        String[] lastNames = new String[] { "Hadyniak", "Legeza", "Pogorelov"};
        int[]  exps = new int[]{5, 30, 3};
        long[]  mainSubIds = new long[]{1L, 2L, 3L};
        when(mockResultSet.getObject(anyString())).thenAnswer(new Answer() {
            private int countIds, countFirstNames, countLastNames, countExp, countMainSubIds = 0;
            public Object answer(InvocationOnMock invocation) {
                switch ((String) invocation.getArguments()[0]) {
                    case "id":              return ids[countIds++];
                    case "firstName":       return firstNames[countFirstNames++];
                    case "lastName":        return lastNames[countLastNames++];
                    case "experience":      return exps[countExp++];
                    case "mainSubjectId":   return mainSubIds[countMainSubIds++];
                }
                return null;
            }
        });
        List<Teacher> list = teacherDAO.getEntityList();
        List<Teacher> expectedList = new ArrayList<Teacher>() {{
            add(new Teacher(ids[0], firstNames[0], lastNames[0], exps[0], mainSubIds[0]));
            add(new Teacher(ids[1], firstNames[1], lastNames[1], exps[1], mainSubIds[1]));
            add(new Teacher(ids[2], firstNames[2], lastNames[2], exps[2], mainSubIds[2]));
        }};
        assertEquals(list.size(), expectedList.size());
        for (int i = 0; i < list.size(); i++) {
            System.out.println(i);
            assertEquals(list.get(i), expectedList.get(i));
        }
    }
}
