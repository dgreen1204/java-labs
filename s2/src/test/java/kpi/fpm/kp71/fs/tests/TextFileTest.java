package kpi.fpm.kp71.fs.tests;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import kpi.fpm.kp71.fs.*;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Text file")
public class TextFileTest {
    @Test
    @DisplayName("Create file")
    void createFileTest() {
        assertThrows(IllegalArgumentException.class,
                () -> TextFile.create("test1", null, "data"));

        Directory dir = Directory.create("some", null);
        assertThrows(IllegalArgumentException.class,
                () -> TextFile.create("name/name", dir, null));

        TextFile file = TextFile.create("file sample", dir, "line");
        assertNotNull(file);
        assertEquals(FSNode.FSNodeType.TEXT_FILE, file.getType());
        assertEquals("file sample", file.getName());
        assertFalse(file.isDirectory());
    }

    @Test
    @DisplayName("Read append")
    void readAppendTest() {
        Directory dir = Directory.create("dir", null);
        TextFile file = TextFile.create("file", dir, "data some");

        assertEquals("data some\n", file.read());
        file.append("another data");
        assertEquals("data some\nanother data\n", file.read());
        file.append("123", 0);
        assertEquals("123\ndata some\nanother data\n", file.read());
        assertThrows(IllegalArgumentException.class,
                () -> file.append(null, 2));
        assertThrows(IllegalArgumentException.class,
                () -> file.append("another data", -2));
        assertThrows(IllegalArgumentException.class,
                () -> file.append(null, -2));
    }

    @Test
    @DisplayName("Delete")
    void deleteTest() {
        Directory dir = Directory.create("dir", null);
        TextFile file = TextFile.create("file", dir, "data some");

        assertEquals("data some\n", file.read());
        file.append("another data");
        assertEquals("data some\nanother data\n", file.read());
        file.append("123", 0);
        assertEquals("123\ndata some\nanother data\n", file.read());
        file.delete(0);
        assertEquals("data some\nanother data\n", file.read());
        file.delete(1);
        assertEquals("data some\n", file.read());
        assertThrows(IllegalArgumentException.class,
                () -> file.delete(-2));
    }

    @Test
    @DisplayName("Edit")
    void editTest() {
        Directory dir = Directory.create("dir", null);
        TextFile file = TextFile.create("file", dir, "data some");

        assertEquals("data some\n", file.read());
        file.append("12343434");
        assertEquals("data some\n12343434\n", file.read());
        file.edit("Text", 0);
        assertEquals("Text\n12343434\n", file.read());
        file.edit("Numbers", 1);
        assertEquals("Text\nNumbers\n", file.read());
        assertThrows(IllegalArgumentException.class,
                () -> file.edit(null, 2));
        assertThrows(IllegalArgumentException.class,
                () -> file.edit("another data", -2));
        assertThrows(IllegalArgumentException.class,
                () -> file.edit(null, -2));
    }
}
