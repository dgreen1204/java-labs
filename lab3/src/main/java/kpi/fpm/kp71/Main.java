package kpi.fpm.kp71;

import static  kpi.fpm.kp71.functionalSet.FunctionalSetUtils.*;

public class Main {
    public static void main(String[] args) {
        System.out.println("Lab 3");
        System.out.println("Check if set of (10, 15) contains 10:");
        System.out.println(union(singletonSet(10), singletonSet(15)).contains(10));

        System.out.println("Check if union contains all numbers in two sets:");
        int[] numbers = new int [] { -100, 100 };
        var unionNumbers = union(singletonSet(numbers[0]), singletonSet(numbers[1]));
        System.out.println(unionNumbers.contains(numbers[0]));
        System.out.println(unionNumbers.contains(numbers[1]));

        System.out.println("Check mapped from previous set");
        var stringSet = map(unionNumbers, Object::toString);
        System.out.println(stringSet.contains(""));
        System.out.println(stringSet.contains("100"));
        System.out.println(stringSet.contains("-100"));

        System.out.println("Check if exists numbers in mapped set");
        var mappedSetAnother = map(unionNumbers, x -> x / 10);
        System.out.println(exists(mappedSetAnother, x -> x > -20 && x < 10));
    }
}
