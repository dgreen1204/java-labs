package kpi.fpm.kp.kp71.models;

import kpi.fpm.kp.kp71.annotations.Column;
import kpi.fpm.kp.kp71.annotations.Entity;
import kpi.fpm.kp.kp71.annotations.ForeignKey;
import kpi.fpm.kp.kp71.annotations.PrimaryKey;

import java.util.Date;
import java.util.Objects;

@Entity(table = "Students")
public class Student {
    @Column(name = "id")
    @PrimaryKey
    private long id;

    @Column(name = "firstname")
    private String firstName;

    @Column(name = "lastname")
    private String lastName;

    @Column(name = "birthday")
    private Date birthday;

    @Column(name = "group_id")
    @ForeignKey(tableRef = "Groups", keyRef = "id")
    private long groupId;

    public Student() {
    }

    public Student(long id, String firstName, String lastName, Date birthday, long groupId) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
        this.groupId = groupId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public long getGroupId() {
        return groupId;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Student)) return false;
        Student student = (Student) o;
        return getId() == student.getId() &&
                getGroupId() == student.getGroupId() &&
                getFirstName().equals(student.getFirstName()) &&
                getLastName().equals(student.getLastName()) &&
                getBirthday().equals(student.getBirthday());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getFirstName(), getLastName(), getBirthday(), getGroupId());
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthday=" + birthday +
                ", groupId=" + groupId +
                '}';
    }
}
