package kpi.fpm.kp.kp71.workers;

import org.javatuples.Pair;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class SQLQueryBuilder {
    public static String buildSelectQuery(Class<?> clazz, Long id) throws Exception {
        StringBuilder query = new StringBuilder("SELECT ");
        String tableName = AnnotationWorker.getTableName(clazz);
        query.append(buildSelectQueryForColumns(clazz, id != null));
        query.append("FROM ");
        query.append(tableName);
        query.append(' ');
        ArrayList<String> inheritedTableNames = new ArrayList<>();
        while (AnnotationWorker.isEntityInherited(clazz)) {
            clazz = clazz.getSuperclass();
            inheritedTableNames.add(AnnotationWorker.getTableName(clazz));
        }
        String primaryKey = AnnotationWorker.getColumnName(AnnotationWorker.getAllColumns(clazz).getValue0());
        if (!inheritedTableNames.isEmpty()) {
            inheritedTableNames.add(0, tableName);
            query.append("INNER JOIN ");
            for (int i = 1; i < inheritedTableNames.size(); i++) {
                query.append(inheritedTableNames.get(i));
                query.append(" on ");
                query.append(inheritedTableNames.get(i - 1));
                query.append('.');
                query.append(primaryKey);
                query.append(" = ");
                query.append(inheritedTableNames.get(i));
                query.append('.');
                query.append(primaryKey);
                query.append(' ');
            }
        }
        if (id != null) {
            query.append("WHERE ");
            query.append(tableName);
            query.append('.');
            query.append(primaryKey);
            query.append(" = ");
            query.append(id);
        }
        return query.toString();
    }

    public static String buildSelectQueryForColumns(Class<?> clazz, boolean getById) throws Exception {
        StringBuilder query = new StringBuilder();
        String tableName = AnnotationWorker.getTableName(clazz);
        Pair<Field, Field[]> result = AnnotationWorker.getAllColumns(clazz);
        Field[] columns = result.getValue1();
        if (!getById && result.getValue0() != null) {
            Field[] temp = columns;
            columns = new Field[columns.length + 1];
            System.arraycopy(temp, 0, columns, 0, temp.length);
            columns[temp.length] = result.getValue0();
        }
        String[] columnNames = AnnotationWorker.getAllColumnNames(columns);
        boolean isInherited = AnnotationWorker.isEntityInherited(clazz);
        for (int i = 0; i < columnNames.length; i++) {
            String fieldName = columns[i].getName();
            query.append(tableName);
            query.append('.');
            query.append(columnNames[i]);
            query.append(' ');
            query.append('"');
            query.append(fieldName);
            query.append('"');
            if (i != columnNames.length - 1)
                query.append(", ");
             else
                query.append(' ');

        }
        if (isInherited) {
            query.append(", ");
            query.append(buildSelectQueryForColumns(clazz.getSuperclass(), getById));
        }
        return query.toString();
    }
}
