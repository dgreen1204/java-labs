package kpi.fpm.kp.kp71;

import kpi.fpm.kp.kp71.dao.DAOImpl;
import kpi.fpm.kp.kp71.models.Curator;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

class CuratorDAOImplTest  extends DAOImplTest {
    @Test
    @DisplayName("getEntityById")
    void testGetCuratorById() throws SQLException {
        DAOImpl<Curator> curatorDAO = new DAOImpl<>(Curator.class, mockConnection);
        when(mockResultSet.next()).thenReturn(true, false);
        Curator curatorId1 = new Curator(1, "Ruslan",
                "Hadyniak", 5, 1L, 2L);
        when(mockResultSet.getObject(anyString())).thenAnswer(new Answer() {
            public Object answer(InvocationOnMock invocation) {
                switch ((String) invocation.getArguments()[0]) {
                    case "id":              return curatorId1.getId();
                    case "firstName":       return curatorId1.getFirstName();
                    case "lastName":        return curatorId1.getLastName();
                    case "experience":      return curatorId1.getExperience();
                    case "mainSubjectId":   return curatorId1.getMainSubjectId();
                    case "groupId":         return curatorId1.getGroupId();
                }
                return null;
            }
        });
        assertEquals(curatorId1, curatorDAO.getEntity(1));
        assertNull(curatorDAO.getEntity(15));
    }

    @Test
    @DisplayName("getEntityList")
    void testGetCuratorList() throws SQLException {
        DAOImpl<Curator> curatorDAO = new DAOImpl<>(Curator.class, mockConnection);
        when(mockResultSet.next()).thenReturn(false,true, true, true, false);
        assertEquals(curatorDAO.getEntityList().size(), 0);
        long[] ids = new long[] {1L, 2L, 3L};
        String[] firstNames = new String[] { "Ruslan", "Viktor", "Volodymyr"};
        String[] lastNames = new String[] { "Hadyniak", "Legeza", "Pogorelov"};
        int[]  exps = new int[]{5, 30, 3};
        long[]  mainSubIds = new long[]{1L, 2L, 3L};
        long[]  groupIds = new long[]{2L, 1L, 3L};
        when(mockResultSet.getObject(anyString())).thenAnswer(new Answer() {
            private int countIds, countFirstNames, countLastNames, countExp, countMainSubIds, countGroupIds = 0;
            public Object answer(InvocationOnMock invocation) {
                switch ((String) invocation.getArguments()[0]) {
                    case "id":              return ids[countIds++];
                    case "firstName":       return firstNames[countFirstNames++];
                    case "lastName":        return lastNames[countLastNames++];
                    case "experience":      return exps[countExp++];
                    case "mainSubjectId":   return mainSubIds[countMainSubIds++];
                    case "groupId":         return groupIds[countGroupIds++];
                }
                return null;
            }
        });
        List<Curator> list = curatorDAO.getEntityList();
        List<Curator> expectedList = new ArrayList<Curator>() {{
            add(new Curator(ids[0], firstNames[0], lastNames[0], exps[0], mainSubIds[0], groupIds[0]));
            add(new Curator(ids[1], firstNames[1], lastNames[1], exps[1], mainSubIds[1], groupIds[1]));
            add(new Curator(ids[2], firstNames[2], lastNames[2], exps[2], mainSubIds[2], groupIds[2]));
        }};
        assertEquals(list.size(), expectedList.size());
        for (int i = 0; i < list.size(); i++) {
            assertEquals(list.get(i), expectedList.get(i));
        }
    }
}
