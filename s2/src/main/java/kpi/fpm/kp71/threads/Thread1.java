package kpi.fpm.kp71.threads;

import kpi.fpm.kp71.fs.*;
import java.util.concurrent.CyclicBarrier;

public class Thread1 extends ThreadTask {
    public Thread1(Directory root) {
        super(root);
    }

    @Override
    public void run() {
        try {
            task_init();
            barrier.await();
            task();
            countDownLatch_2.await();
            task_finalize();
            countDownLatch_4.countDown();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void task_init() throws InterruptedException {
        System.out.println("Thread 1 init");
        Directory dir = Directory.create("thread1Dir", root);

        for (int i = 0; i < 5; i++) {
            BufferFile.create("test" + i, root);
            Thread.sleep(100);
        }
    }

    public void task() throws InterruptedException {
        System.out.println("Thread 1 task");
        for (int i = 0; i < 5; i++) {
            BufferFile file = (BufferFile) root.getChild("test" + i);
            file.move((Directory) root.getChild("lvl1"));
            Thread.sleep(100);
        }
    }

    public void task_finalize() {
        System.out.println("Thread 1 finalize");
    }
}