package kpi.fpm.kp.kp71.models;

import kpi.fpm.kp.kp71.annotations.Column;
import kpi.fpm.kp.kp71.annotations.Entity;
import kpi.fpm.kp.kp71.annotations.PrimaryKey;

import java.util.Objects;

@Entity(table = "Subjects")
public class Subject {
    @Column(name = "id")
    @PrimaryKey
    private long id;

    @Column(name = "title")
    private String title;

    public Subject() {
    }

    public Subject(long id, String title) {
        this.id = id;
        this.title = title;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Subject)) return false;
        Subject subject = (Subject) o;
        return getId() == subject.getId() &&
                getTitle().equals(subject.getTitle());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getTitle());
    }

    @Override
    public String toString() {
        return "Subject{" +
                "id=" + id +
                ", title='" + title + '\'' +
                '}';
    }
}

