package kpi.fpm.kp71.fs;

public class BinaryFile extends FSNode {
    protected byte[] data;

    private BinaryFile(String name, Directory parent, byte[] data) {
        super(name, parent);
        this.data = data;
        this.fileType = FSNodeType.BINARY_FILE;
    }

    public static BinaryFile create(String name, Directory parent, byte[] data) {
        checkNull(data, "Could not create file with null data!");
        return new BinaryFile(name, parent, data);
    }

    public byte[] read() {
        return readonly(() -> data.clone());
    }
}
