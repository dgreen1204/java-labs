package kpi.fpm.kp.kp71;

import kpi.fpm.kp.kp71.dao.DAOImpl;
import kpi.fpm.kp.kp71.models.Student;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@DisplayName("Student")
@ExtendWith(MockitoExtension.class)
class StudentDAOImplTest extends DAOImplTest {
    @Test
    @DisplayName("getEntityById")
    void testGetStudentById() throws SQLException, ParseException {
        DAOImpl<Student> studentDAO = new DAOImpl<>(Student.class, mockConnection);
        when(mockResultSet.next()).thenReturn(true, false);
        SimpleDateFormat p = new SimpleDateFormat("dd.MM.yyyy");
        Date arkasha_bd = new Date(p.parse("21.09.1999").getTime());
        Student studentId1 = new Student(1, "Arkasha", "Kravchuk", arkasha_bd, 1L);
        when(mockResultSet.getObject(anyString())).thenAnswer(new Answer() {
            public Object answer(InvocationOnMock invocation) {
                switch ((String) invocation.getArguments()[0]) {
                    case "id":             return studentId1.getId();
                    case "firstName":      return studentId1.getFirstName();
                    case "lastName":       return studentId1.getLastName();
                    case "birthday":       return studentId1.getBirthday();
                    case "groupId":        return studentId1.getGroupId();
                }
                return null;
            }
        });
        assertEquals(studentId1, studentDAO.getEntity(1));
        assertNull(studentDAO.getEntity(15));
    }

    @Test
    @DisplayName("getEntityList")
    void testGetStudentList() throws SQLException, ParseException {
        DAOImpl<Student> studentDAO = new DAOImpl<>(Student.class, mockConnection);
        when(mockResultSet.next()).thenReturn(false,true, true, true, false);
        assertEquals(studentDAO.getEntityList().size(), 0);
        SimpleDateFormat p = new SimpleDateFormat("dd.MM.yyyy");
        Date arkasha_bd = new Date(p.parse("21.09.1999").getTime());
        Date anna_bd = new Date(p.parse("09.06.2000").getTime());
        Date tesla_bd = new Date(p.parse("21.09.2002").getTime());
        long[] ids = new long[] {1L, 2L, 3L};
        String[] firstNames = new String[] { "Arkasha", "Anna", "Tesla" };
        String[] lastNames = new String[] {"Kravchuk", "Arnautova", "Tesla"};
        long[] groupIds = new long[] {1L, 2L, 3L};
        Date[] birthdays = new Date[] { arkasha_bd, anna_bd, tesla_bd };
        when(mockResultSet.getObject(anyString())).thenAnswer(new Answer() {
            private int countIds, countFirstNames, countLastNames, countBD, countGroupIds = 0;
            public Object answer(InvocationOnMock invocation) {
                switch ((String) invocation.getArguments()[0]) {
                    case "id":             return ids[countIds++];
                    case "firstName":      return firstNames[countFirstNames++];
                    case "lastName":       return lastNames[countLastNames++];
                    case "birthday":       return birthdays[countBD++];
                    case "groupId":        return groupIds[countGroupIds++];
                }
                return null;
            }
        });
        List<Student> list = studentDAO.getEntityList();
        List<Student> expectedList = new ArrayList<Student>() {{
            add(new Student(ids[0], firstNames[0], lastNames[0], birthdays[0], groupIds[0]));
            add(new Student(ids[1], firstNames[1], lastNames[1], birthdays[1], groupIds[1]));
            add(new Student(ids[2], firstNames[2], lastNames[2], birthdays[2], groupIds[2]));
        }};
        assertEquals(list.size(), expectedList.size());
        for (int i = 0; i < list.size(); i++) {
            assertEquals(list.get(i), expectedList.get(i));
        }
    }
}
