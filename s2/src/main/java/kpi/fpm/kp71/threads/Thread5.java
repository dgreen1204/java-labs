package kpi.fpm.kp71.threads;

import kpi.fpm.kp71.fs.*;
import java.util.concurrent.CyclicBarrier;

public class Thread5 extends ThreadTask {
    @Override
    public void run() {
        try {
            task_init();
            barrier.await();
            task();
            countDownLatch_4.await();
            task_finalize();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public Thread5(Directory root) {
        super(root);
    }

    public void task_init() {
        System.out.println("Thread 5 init");
    }

    public void task() {
        System.out.println("Thread 5 task");
    }

    public void task_finalize() {
        System.out.println("Thread 5 finalize");
    }
}