package kpi.fpm.kp71;

import kpi.fpm.kp71.fs.*;
import kpi.fpm.kp71.threads.*;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;

public class Main {
    public static void main(String[] args) throws BrokenBarrierException, InterruptedException {
        System.out.println("Lab s2 1");
        Directory root = Directory.create("root", null);
        Directory lvlOneDir = Directory.create("lvl1", root);
        BinaryFile binaryFile = BinaryFile.create("binary", lvlOneDir, "data".getBytes());
        BufferFile<Integer> bufferFile = BufferFile.<Integer>create("buffer", lvlOneDir);
        LogTextFile logTextFile = LogTextFile.create("logTextFile", lvlOneDir, "data");
        TextFile textFile = TextFile.create("rextfilename", lvlOneDir);

        new Thread(new Thread1(root), "1").start();
        new Thread(new Thread2(root), "2").start();
        new Thread(new Thread3(root), "3").start();
        new Thread(new Thread4(root), "4").start();
        new Thread(new Thread5(root), "5").start();

    }
}
