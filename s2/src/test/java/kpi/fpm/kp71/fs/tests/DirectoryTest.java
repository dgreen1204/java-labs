package kpi.fpm.kp71.fs.tests;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import kpi.fpm.kp71.fs.*;

import java.nio.file.DirectoryNotEmptyException;
import java.util.ArrayList;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Directory")
public class DirectoryTest {
    @Test
    @DisplayName("Directory creation")
    void createDirectoryTest() {
        Directory root = Directory.create("test", null);
        assertThrows(IllegalArgumentException.class,
                () -> Directory.create("", root));

        Directory dir = Directory.create("some", root);
        assertNotNull(dir);

        assertEquals(FSNode.FSNodeType.DIRECTORY, dir.getType());
        assertEquals("some", dir.getName());
        assertTrue(dir.isDirectory());

        Directory subDir = Directory.create("tmp", dir);
        assertEquals("tmp", subDir.getName());
    }

    @Test
    @DisplayName("list directory")
    void listDirectoryTest() {
        Directory root = Directory.create("roo1", null);
        ArrayList<FSNode> listElements = new ArrayList<FSNode>();
        listElements.add(Directory.create("sub", root));
        listElements.add(BinaryFile.create("file.bin", root, "data".getBytes()));
        listElements.add(LogTextFile.create("log", root, "line"));
        assertEquals(listElements, root.list());
        assertEquals(Collections.emptyList(), Directory.create("", null).list());
    }

    @Test
    @DisplayName("delete directory")
    void deleteDirectoryTest() {
        Directory root = Directory.create("", null);
        BinaryFile file = BinaryFile.create("file", root, "data".getBytes());
        Directory subDir = Directory.create("var", root);

        assertDoesNotThrow(() -> root.delete());
        assertDoesNotThrow(() -> subDir.delete());
        assertDoesNotThrow(() -> file.delete());
    }

    @Test
    @DisplayName("move directory")
    void moveSubdirectoriesAndFilesTest() {
        Directory root = Directory.create("", null);
        Directory subDir = Directory.create("folder", root);
        Directory anotherSubDir = Directory.create("dir", subDir);
        LogTextFile log = LogTextFile.create("l.txt", subDir, "\n");

        assertThrows(IllegalArgumentException.class, () -> root.move(subDir));
        assertTrue(subDir.list().contains(anotherSubDir));
        assertTrue(subDir.list().contains(log));
    }
}
