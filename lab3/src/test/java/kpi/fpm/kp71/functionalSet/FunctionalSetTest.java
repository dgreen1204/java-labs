package kpi.fpm.kp71.functionalSet;
import kpi.fpm.kp71.functionalSet.FunctionalSetUtils;
import kpi.fpm.kp71.functionalSet.PurelyFunctionalSet;
import org.junit.Assert;
import org.junit.Test;


public class FunctionalSetTest {

    public FunctionalSetTest() {
    }

    @Test
    public void createEmptyIntegerSet_shouldNotContainsAnyValue() {
        PurelyFunctionalSet<Integer> empty = FunctionalSetUtils.empty();

        for (int i = -500; i < 500; i++) {
            Assert.assertFalse(empty.contains(i));
        }
    }

    @Test
    public void createEmptyObjectSet_shouldNotContainsAnyValue() {
        PurelyFunctionalSet<Object> empty = FunctionalSetUtils.empty();
        Assert.assertFalse(empty.contains(new Object()));
    }

    @Test
    public void createSingletonIntegerSet_shouldContainsOneIntegerValue() {
        Integer value = 10;
        PurelyFunctionalSet<Integer> set = FunctionalSetUtils.singletonSet(value);
        Assert.assertTrue(set.contains(value));
        Assert.assertFalse(set.contains(value + 17));
        Assert.assertFalse(set.contains(value - 5));
        Assert.assertFalse(set.contains(value + 7));
    }

    @Test
    public void createSingletonObjectSet_shouldContainsOneObjectValue() {
        Object obj = new Object();
        PurelyFunctionalSet<Object> set = FunctionalSetUtils.singletonSet(obj);
        Assert.assertFalse(set.contains(new Object()));
        Assert.assertTrue(set.contains(obj));
    }

    @Test
    public void unionTwoSingletonIntegerSet_shouldContainsTwoValues() {
        Integer val1 = 10;
        Integer val2 = 20;
        PurelyFunctionalSet<Integer> set1 = FunctionalSetUtils.singletonSet(val1);
        PurelyFunctionalSet<Integer> set2 = FunctionalSetUtils.singletonSet(val2);

        PurelyFunctionalSet<Integer> set = FunctionalSetUtils.union(set1, set2);

        Assert.assertTrue(set.contains(val1));
        Assert.assertTrue(set.contains(val2));
        Assert.assertFalse(set.contains(-10));
        Assert.assertFalse(set.contains(0));
        Assert.assertFalse(set.contains(30));
    }

    @Test
    public void unionTwoSingletonObjectSet_shouldContainsTwoObjectValue() {
        Object obj1 = new Object();
        Object obj2 = new Object();
        PurelyFunctionalSet<Object> set1 = FunctionalSetUtils.singletonSet(obj1);
        PurelyFunctionalSet<Object> set2 = FunctionalSetUtils.singletonSet(obj2);

        PurelyFunctionalSet<Object> set = FunctionalSetUtils.union(set1, set2);
        Assert.assertTrue(set.contains(obj1));
        Assert.assertTrue(set.contains(obj2));
        Assert.assertFalse(set.contains(new Object()));
    }

    @Test
    public void intersectTwoIntegerSet_shouldContainsOneValue() {
        Integer val1 = 10;
        Integer val2 = 25;
        PurelyFunctionalSet<Integer> set1 = FunctionalSetUtils.singletonSet(val1);
        PurelyFunctionalSet<Integer> set2 = FunctionalSetUtils.singletonSet(val2);
        PurelyFunctionalSet<Integer> set3 = FunctionalSetUtils.singletonSet(val2);

        PurelyFunctionalSet<Integer> set4 = FunctionalSetUtils.union(set1, set2);
        PurelyFunctionalSet<Integer> set = FunctionalSetUtils.intersect(set4, set3);

        Assert.assertFalse(set.contains(val1));
        Assert.assertTrue(set.contains(val2));
    }

    @Test
    public void intersectTwoObjectSet_shouldContainsOneValue() {
        Object obj1 = new Object();
        Object obj2 = new Object();
        PurelyFunctionalSet<Object> set1 = FunctionalSetUtils.singletonSet(obj1);
        PurelyFunctionalSet<Object> set2 = FunctionalSetUtils.singletonSet(obj2);
        PurelyFunctionalSet<Object> set3 = FunctionalSetUtils.singletonSet(obj2);

        PurelyFunctionalSet<Object> set4 = FunctionalSetUtils.union(set1, set2);
        PurelyFunctionalSet<Object> set = FunctionalSetUtils.intersect(set4, set3);

        Assert.assertFalse(set.contains(obj1));
        Assert.assertTrue(set.contains(obj2));
    }

    @Test
    public void diffTwoIntegerSet_shouldContainsOneValue() {
        Integer val1 = 10;
        Integer val2 = 20;
        PurelyFunctionalSet<Integer> set1 = FunctionalSetUtils.singletonSet(val1);
        PurelyFunctionalSet<Integer> set2 = FunctionalSetUtils.singletonSet(val2);
        PurelyFunctionalSet<Integer> set3 = FunctionalSetUtils.singletonSet(val2);

        PurelyFunctionalSet<Integer> set4 = FunctionalSetUtils.union(set1, set2);
        PurelyFunctionalSet<Integer> set = FunctionalSetUtils.diff(set4, set3);

        Assert.assertTrue(set.contains(val1));
        Assert.assertFalse(set.contains(val2));
    }

    @Test
    public void diffTwoObjectSet_shouldContainsOneValue() {
        Object obj1 = new Object();
        Object obj2 = new Object();
        PurelyFunctionalSet<Object> set1 = FunctionalSetUtils.singletonSet(obj1);
        PurelyFunctionalSet<Object> set2 = FunctionalSetUtils.singletonSet(obj2);
        PurelyFunctionalSet<Object> set3 = FunctionalSetUtils.singletonSet(obj2);

        PurelyFunctionalSet<Object> set4 = FunctionalSetUtils.union(set1, set2);
        PurelyFunctionalSet<Object> set = FunctionalSetUtils.diff(set4, set3);

        Assert.assertTrue(set.contains(obj1));
        Assert.assertFalse(set.contains(obj2));
    }

    @Test
    public void filterIntegerSet_shouldContainsEvenValues() {
        Integer val1 = 15;
        Integer val2 = 20;
        Integer val3 = 38;
        Integer val4 = 46;
        PurelyFunctionalSet<Integer> set1 = FunctionalSetUtils.singletonSet(val1);
        PurelyFunctionalSet<Integer> set2 = FunctionalSetUtils.singletonSet(val2);
        PurelyFunctionalSet<Integer> set3 = FunctionalSetUtils.singletonSet(val3);
        PurelyFunctionalSet<Integer> set4 = FunctionalSetUtils.singletonSet(val4);

        PurelyFunctionalSet<Integer> set5 = FunctionalSetUtils.union(set1, set2);
        PurelyFunctionalSet<Integer> set6 = FunctionalSetUtils.union(set3, set4);
        PurelyFunctionalSet<Integer> set7 = FunctionalSetUtils.union(set5, set6);

        PurelyFunctionalSet<Integer> set = FunctionalSetUtils.filter(set7, x -> x % 2 == 0);

        Assert.assertFalse(set.contains(val1));
        Assert.assertTrue(set.contains(val2));
        Assert.assertTrue(set.contains(val3));
        Assert.assertTrue(set.contains(val4));
    }

    @Test
    public void forAllIntegerSetWithIsEvenPredicate_shouldReturnTrue() {
        Integer val1 = 28;
        Integer val2 = 2;
        Integer val3 = 694;
        Integer val4 = 876;
        PurelyFunctionalSet<Integer> set1 = FunctionalSetUtils.singletonSet(val1);
        PurelyFunctionalSet<Integer> set2 = FunctionalSetUtils.singletonSet(val2);
        PurelyFunctionalSet<Integer> set3 = FunctionalSetUtils.singletonSet(val3);
        PurelyFunctionalSet<Integer> set4 = FunctionalSetUtils.singletonSet(val4);

        PurelyFunctionalSet<Integer> set5 = FunctionalSetUtils.union(set1, set2);
        PurelyFunctionalSet<Integer> set6 = FunctionalSetUtils.union(set3, set4);
        PurelyFunctionalSet<Integer> set7 = FunctionalSetUtils.union(set5, set6);

        boolean res = FunctionalSetUtils.forAll(set7, x -> x % 2 == 0);

        Assert.assertTrue(res);
    }

    @Test
    public void forAllIntegerSetWithIsEvenPredicate_shouldReturnFalse() {
        Integer val1 = 153;
        Integer val2 = 43;
        Integer val3 = 6;
        Integer val4 = 8;
        PurelyFunctionalSet<Integer> set1 = FunctionalSetUtils.singletonSet(val1);
        PurelyFunctionalSet<Integer> set2 = FunctionalSetUtils.singletonSet(val2);
        PurelyFunctionalSet<Integer> set3 = FunctionalSetUtils.singletonSet(val3);
        PurelyFunctionalSet<Integer> set4 = FunctionalSetUtils.singletonSet(val4);

        PurelyFunctionalSet<Integer> set5 = FunctionalSetUtils.union(set1, set2);
        PurelyFunctionalSet<Integer> set6 = FunctionalSetUtils.union(set3, set4);
        PurelyFunctionalSet<Integer> set7 = FunctionalSetUtils.union(set5, set6);

        boolean res = FunctionalSetUtils.forAll(set7, x -> x % 2 == 0);

        Assert.assertFalse(res);
    }

    @Test
    public void existsIntegerSetWithIsEvenPredicate_shouldReturnTrue() {
        Integer val1 = 71;
        Integer val2 = 38;
        Integer val3 = 66;
        Integer val4 = 27;
        PurelyFunctionalSet<Integer> set1 = FunctionalSetUtils.singletonSet(val1);
        PurelyFunctionalSet<Integer> set2 = FunctionalSetUtils.singletonSet(val2);
        PurelyFunctionalSet<Integer> set3 = FunctionalSetUtils.singletonSet(val3);
        PurelyFunctionalSet<Integer> set4 = FunctionalSetUtils.singletonSet(val4);

        PurelyFunctionalSet<Integer> set5 = FunctionalSetUtils.union(set1, set2);
        PurelyFunctionalSet<Integer> set6 = FunctionalSetUtils.union(set3, set4);
        PurelyFunctionalSet<Integer> set7 = FunctionalSetUtils.union(set5, set6);

        boolean res = FunctionalSetUtils.exists(set7, x -> x % 2 == 0);

        Assert.assertTrue(res);
    }

    @Test
    public void existsIntegerSetWithIsEvenPredicate_shouldReturnFalse() {
        Integer val1 = 91;
        Integer val2 = 83;
        Integer val3 = 65;
        Integer val4 = 7;
        PurelyFunctionalSet<Integer> set1 = FunctionalSetUtils.singletonSet(val1);
        PurelyFunctionalSet<Integer> set2 = FunctionalSetUtils.singletonSet(val2);
        PurelyFunctionalSet<Integer> set3 = FunctionalSetUtils.singletonSet(val3);
        PurelyFunctionalSet<Integer> set4 = FunctionalSetUtils.singletonSet(val4);

        PurelyFunctionalSet<Integer> set5 = FunctionalSetUtils.union(set1, set2);
        PurelyFunctionalSet<Integer> set6 = FunctionalSetUtils.union(set3, set4);
        PurelyFunctionalSet<Integer> set7 = FunctionalSetUtils.union(set5, set6);

        boolean res = FunctionalSetUtils.exists(set7, x -> x % 2 == 0);

        Assert.assertFalse(res);
    }

    @Test
    public void mapIntegerSet_shouldReturnSetThatMultiplyBy2() {
        Integer val1 = 61;
        Integer val2 = 63;
        Integer val3 = 75;
        Integer val4 = 77;
        PurelyFunctionalSet<Integer> set1 = FunctionalSetUtils.singletonSet(val1);
        PurelyFunctionalSet<Integer> set2 = FunctionalSetUtils.singletonSet(val2);
        PurelyFunctionalSet<Integer> set3 = FunctionalSetUtils.singletonSet(val3);
        PurelyFunctionalSet<Integer> set4 = FunctionalSetUtils.singletonSet(val4);

        PurelyFunctionalSet<Integer> set5 = FunctionalSetUtils.union(set1, set2);
        PurelyFunctionalSet<Integer> set6 = FunctionalSetUtils.union(set3, set4);
        PurelyFunctionalSet<Integer> set7 = FunctionalSetUtils.union(set5, set6);

        PurelyFunctionalSet<Integer> set = FunctionalSetUtils.map(set7, x -> x * 2);

        Assert.assertTrue(set.contains(val1 * 2));
        Assert.assertTrue(set.contains(val2 * 2));
        Assert.assertTrue(set.contains(val3 * 2));
        Assert.assertTrue(set.contains(val4 * 2));
    }
}
