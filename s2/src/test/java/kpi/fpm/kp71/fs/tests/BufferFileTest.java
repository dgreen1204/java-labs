package kpi.fpm.kp71.fs.tests;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import kpi.fpm.kp71.fs.*;

import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Buffer file")
public class BufferFileTest {
    @Test
    @DisplayName("File creation")
    void createFileTest() {
        assertThrows(IllegalArgumentException.class,
                () -> BufferFile.create("test1", null));

        Directory dir1 = Directory.create("dir1", null);
        assertThrows(IllegalArgumentException.class, () -> BufferFile.create("", dir1));

        BufferFile file = BufferFile.create("buff file", dir1);
        assertNotNull(file);
        assertEquals(FSNode.FSNodeType.BUFFER_FILE, file.getType());
        assertEquals("buff file", file.getName());
        assertFalse(file.isDirectory());
    }

    @Test
    @DisplayName("Consume push")
    void consumePushTest() {
        Directory dir = Directory.create("dir", null);
        BufferFile file = BufferFile.<Integer>create("file", dir);

        assertThrows(NoSuchElementException.class, () -> file.consume());
        file.push(1);
        file.push(2);
        assertEquals(1, file.consume());
        assertEquals(2, file.consume());
        assertThrows(NoSuchElementException.class, () -> file.consume());
        assertThrows(IllegalArgumentException.class, () -> file.push(null));
    }

    @Test
    @DisplayName("Push many elements")
    void pushManyElementsTest() {
        Directory dir = Directory.create("dir", null);
        BufferFile<Integer> file = BufferFile.<Integer>create("file", dir);

        for (int i = 0; i < 10; i++) {
            file.push(i);
        }
        assertThrows(IllegalStateException.class, () -> file.push(126));
    }
}
