package kpi.fpm.kp71.fs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Directory extends FSNode {
    private static final int DIR_MAX_ELEMS = 50;
    private Map<String, FSNode> children;

    private Directory(String name) {
        super(name);
        this.fileType = FSNodeType.DIRECTORY;
        children = new HashMap<>(DIR_MAX_ELEMS);
    }

    private Directory(String name, Directory parent) {
        super(name, parent);
        this.fileType = FSNodeType.DIRECTORY;
        children = new HashMap<>(DIR_MAX_ELEMS);
    }

    public boolean containsChild(String childName) {
        return readonly(() -> children.containsKey(childName));
    }

    public int countChildren() {
        return readonly(() -> children.size());
    }

    public FSNode getChild(String childName) {
        return readonly(() -> children.get(childName));
    }

    public void appendChild(FSNode child) {
        readwrite(() -> {
            if (countChildren() >= DIR_MAX_ELEMS) {
                throw new IllegalStateException("Directory can't contain more than 50 children");
            }
            children.put(child.getName(), child);
        });
    }

    public FSNode removeChild(String childName) {
       return readwrite(() -> children.remove(childName));
    }

    public void updateChild(FSNode child, String oldName) {
        readwrite(() -> {
            children.put(child.getName(), removeChild(oldName));
        });
    }

    public List<FSNode> getChildren() {
        return readonly(() -> new ArrayList<>(children.values()));
    }

    public static Directory create(String name, Directory parent) {
        if (parent == null) {
            return new Directory("root");
        }
        return new Directory(name, parent);
    }

    public List<FSNode> list() {
        return getChildren();
    }

    @Override
    public void move(Directory destination) {
        checkNull(getParent(), "Root directory couldn't be moved");
        super.move(destination);
    }
}
