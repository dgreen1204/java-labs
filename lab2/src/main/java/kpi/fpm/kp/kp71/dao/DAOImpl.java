package kpi.fpm.kp.kp71.dao;

import kpi.fpm.kp.kp71.workers.AnnotationWorker;
import kpi.fpm.kp.kp71.workers.SQLQueryBuilder;
import org.javatuples.Pair;
import org.postgresql.ds.PGSimpleDataSource;

import java.io.InputStream;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class DAOImpl<T> implements IDAOImpl<T>  {
    private Class<T> tclass;
    private Connection connection;

    public DAOImpl(Class<T> tclass) throws Exception {
        this.tclass = tclass;
        Properties properties = new Properties();
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream inputStream = classloader.getResourceAsStream("db/db.trueProperties");
        if (inputStream == null) {
            throw new Exception("Could not read database properties");
        }
        properties.load(inputStream);
        PGSimpleDataSource pgDataSource = new PGSimpleDataSource();
        pgDataSource.setServerName(properties.getProperty("DB_HOST"));
        pgDataSource.setDatabaseName(properties.getProperty("DB_NAME"));
        pgDataSource.setPortNumber(Integer.parseInt(properties.getProperty("DB_PORT")));
        pgDataSource.setUser(properties.getProperty("DB_USERNAME"));
        pgDataSource.setPassword(properties.getProperty("DB_PASSWORD"));
        connection = pgDataSource.getConnection();
    }

    public DAOImpl(Class<T> tclass, Connection connection) {
        this.tclass = tclass;
        this.connection = connection;
    }

    private T createNewInstanceAndFillData(ResultSet resultSet, Long id) throws Exception {
        T item = tclass.getDeclaredConstructor().newInstance();
        List<Field> columns = new ArrayList<>();
        Class<? super T> currentClass = tclass;
        Field primaryKey = null;
        while (true) {
            Pair<Field, Field[]> result = AnnotationWorker.getAllColumns(currentClass);
            columns.addAll(Arrays.asList(result.getValue1()));
            if (AnnotationWorker.isEntityInherited(currentClass)) {
                currentClass = currentClass.getSuperclass();
            } else {
                if (id == null)
                    columns.add(result.getValue0());
                 else
                    primaryKey = result.getValue0();
                break;
            }
        }
        for (Field column : columns) {
            column.setAccessible(true);
            Object value = resultSet.getObject(column.getName());
            column.set(item, value);
        }
        if (id != null) {
            primaryKey.setAccessible(true);
            primaryKey.set(item, id);
        }
        return item;
    }

    @Override
    public T getEntity(long id) {
        try {
            String query = SQLQueryBuilder.buildSelectQuery(tclass, id);
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            if (!rs.next()) {
                return null;
            }
            return createNewInstanceAndFillData(rs, id);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<T> getEntityList() {
        ArrayList<T> result = new ArrayList<>();
        try {
            String query =  SQLQueryBuilder.buildSelectQuery(tclass, null);
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while(rs.next()) {
                result.add(createNewInstanceAndFillData(rs, null));
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
