package kpi.fpm.kp71.fs;

public class LogTextFile extends FSNode {
    private StringBuilder data;


    private LogTextFile(String name, Directory parent, String data) {
        super(name, parent);
        this.data = new StringBuilder(data);
        this.fileType = FSNodeType.LOG_TEXT_FILE;
    }

    public static LogTextFile create(String name, Directory parent, String data) {
        checkNull(data, "Could not create file with null data!");
        return new LogTextFile(name, parent, data);
    }

    public String read() {
        return readonly(() -> data.toString());
    }

    public void append(String line) {
        readwrite(() -> {
            checkNull(line, "Could not append null line!");
            data.append(line);
        });
    }

}
