package kpi.fpm.kp71.fs;

import java.util.LinkedList;
import java.util.List;

public class TextFile extends FSNode {
    List<String> rows;


    protected TextFile(String name, Directory parent) {
        super(name, parent);
        this.fileType = FSNodeType.TEXT_FILE;
        this.rows = new LinkedList<String>();
    }

    public static TextFile create(String name, Directory parent) {
        return new TextFile(name, parent);
    }

    public static TextFile create(String name, Directory parent, String firstRow) {
        checkNull(firstRow, "Could not create file with null first row!");
        TextFile newFile = new TextFile(name, parent);
        newFile.append(firstRow);
        return newFile;
    }

    public void append(String row){
        readwrite(() -> {
            checkNull(row,"Could not append null row!");
            rows.add(row);
        });
    }

    public void append(String row, int rowNumber){
        readwrite(() -> {
            checkNull(row,"Could not append null row!");
            checkPosition(rowNumber, "Could not append row to negative position!");
            rows.add(rowNumber, row);
        });
    }

    public String  delete(int rowNumber) {
        return readwrite(() -> {
            checkPosition(rowNumber, "Could not delete row from negative position!");
            return rows.remove(rowNumber);
        });
    }

    public String edit(String newRow, int rowNumber){
        return readwrite(() -> {
            checkNull(newRow,"Could not set null row!");
            checkPosition(rowNumber, "Could not edit row with negative position!");
            return rows.set(rowNumber, newRow);
        });
    }

    public String  read(){
        return readonly(
                () -> rows.stream().reduce("", (left, right) -> left + right + "\n")
        );
    }

    private static void checkPosition(int position, String message) {
        if (position < 0) {
            throw new IllegalArgumentException(message);
        }
    }
}
