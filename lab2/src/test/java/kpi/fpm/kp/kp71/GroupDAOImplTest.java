package kpi.fpm.kp.kp71;

import kpi.fpm.kp.kp71.dao.DAOImpl;
import kpi.fpm.kp.kp71.models.Group;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@DisplayName("Group")
@ExtendWith(MockitoExtension.class)
class GroupDAOImplTest extends DAOImplTest {
    @Test
    @DisplayName("getEntityById")
    void testGetGroupById() throws SQLException, ParseException {
        DAOImpl<Group> groupDAO = new DAOImpl<>(Group.class, mockConnection);
        when(mockResultSet.next()).thenReturn(true, false);
        SimpleDateFormat p = new SimpleDateFormat("dd.MM.yyyy");
        Date entrance2017 = new Date(p.parse("01.09.2017").getTime());
        Group groupId1 = new Group(1, "KP-71", entrance2017);
        when(mockResultSet.getObject(anyString())).thenAnswer(new Answer() {
            public Object answer(InvocationOnMock invocation) {
                switch ((String) invocation.getArguments()[0]) {
                    case "id":              return groupId1.getId();
                    case "code":            return groupId1.getCode();
                    case "entranceYear":    return groupId1.getEntranceYear();
                }
                return null;
            }
        });
        assertEquals(groupId1, groupDAO.getEntity(1));
        assertNull(groupDAO.getEntity(15));
    }

    @Test
    @DisplayName("getEntityList")
    void testGetGroupList() throws SQLException, ParseException {
        DAOImpl<Group> subjectDAO = new DAOImpl<>(Group.class, mockConnection);
        when(mockResultSet.next()).thenReturn(false,true, true, true, false);
        assertEquals(subjectDAO.getEntityList().size(), 0);
        SimpleDateFormat p = new SimpleDateFormat("dd.MM.yyyy");
        Date entrance2017 = new Date(p.parse("01.09.2017").getTime());
        Date entrance2019 = new Date(p.parse("01.09.2019").getTime());
        long[] ids = new long[] {1L, 2L, 3L};
        String[] codes = new String[] {"KP-71", "KP-72", "KP-91"};
        Date[] enterDates = new Date[]{ entrance2017, entrance2017, entrance2019};
        when(mockResultSet.getObject(anyString())).thenAnswer(new Answer() {
            private int countIds, countCodes, countDates = 0;
            public Object answer(InvocationOnMock invocation) {
                switch ((String) invocation.getArguments()[0]) {
                    case "id":              return ids[countIds++];
                    case "code":            return codes[countCodes++];
                    case "entranceYear":    return enterDates[countDates++];
                }
                return null;
            }
        });
        List<Group> list = subjectDAO.getEntityList();
        List<Group> expectedList = new ArrayList<Group>() {{
            add(new Group(ids[0], codes[0], entrance2017));
            add(new Group(ids[1], codes[1], entrance2017));
            add(new Group(ids[2], codes[2], entrance2019));
        }};
        assertEquals(list.size(), expectedList.size());
        for (int i = 0; i < list.size(); i++) {
            assertEquals(list.get(i), expectedList.get(i));
        }
    }
}
