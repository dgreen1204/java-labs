package kpi.fpm.kp71.threads;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import kpi.fpm.kp71.fs.*;

public abstract class ThreadTask implements Runnable {
    Directory root;
    private static int count = 0;
    protected static CyclicBarrier barrier = new CyclicBarrier(5);
    protected static CountDownLatch countDownLatch_2 = new CountDownLatch(2);
    protected static CountDownLatch countDownLatch_4 = new CountDownLatch(4);

    public ThreadTask(Directory root) {
        this.root = root;
    }

    public void task_init() throws InterruptedException {}

    public void task() throws InterruptedException {}

    public void task_finalize() {}
}

