package kpi.fpm.kp71.threads;

import kpi.fpm.kp71.fs.*;
import java.util.concurrent.CyclicBarrier;

public class Thread4 extends ThreadTask {
    public Thread4(Directory root) {
        super(root);
    }

    @Override
    public void run() {
        try {
            task_init();
            barrier.await();
            task();
            task_finalize();
            countDownLatch_2.countDown();
            countDownLatch_4.countDown();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void task_init() {
        System.out.println("Thread 4 init");
    }


    public void task() throws InterruptedException {
        System.out.println("Thread 4 task");
        Thread.sleep(200);
        ((LogTextFile) root.getChild("LogTextFile1")).append("line");
    }

    public void task_finalize() {
        System.out.println("Thread 4 finalize");
        ((LogTextFile) root.getChild("LogTextFile1")).append("line");
        System.out.println(((LogTextFile) root.getChild("LogTextFile1")).read());
    }
}