package kpi.fpm.kp.kp71.models;

import kpi.fpm.kp.kp71.annotations.Column;
import kpi.fpm.kp.kp71.annotations.Entity;
import kpi.fpm.kp.kp71.annotations.EntityInherited;
import kpi.fpm.kp.kp71.annotations.ForeignKey;

import java.util.Objects;

@Entity(table = "Curators")
@EntityInherited
public class Curator extends Teacher {
    @Column(name = "group_id")
    @ForeignKey(tableRef = "Groups", keyRef = "id")
    private long groupId;

    public Curator(){}

    public Curator(long id, String firstName, String lastName, int experience,
                   long mainSubjectId, long groupId) {
        super(id, firstName, lastName, experience, mainSubjectId);
        this.groupId = groupId;
    }

    public long getGroupId() {
        return groupId;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Curator)) return false;
        if (!super.equals(o)) return false;
        Curator curator = (Curator) o;
        return getGroupId() == curator.getGroupId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getGroupId());
    }

    @Override
    public String toString() {
        return "Curator{" +
                "id=" + this.getId() +
                ", firstName='" + this.getFirstName() + '\'' +
                ", lastName='" + this.getLastName() + '\'' +
                ", experience=" + this.getExperience() +
                ", mainSubjectId=" + this.getMainSubjectId() +
                "groupId=" + groupId +
                '}';
    }
}
