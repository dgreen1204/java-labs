package kpi.fpm.kp.kp71.models;

import kpi.fpm.kp.kp71.annotations.Column;
import kpi.fpm.kp.kp71.annotations.Entity;
import kpi.fpm.kp.kp71.annotations.ForeignKey;
import kpi.fpm.kp.kp71.annotations.PrimaryKey;

import java.util.Objects;

@Entity(table = "Teachers")
public class Teacher {
    @Column(name = "id")
    @PrimaryKey
    private long id;

    @Column(name = "firstname")
    private String firstName;

    @Column(name = "lastname")
    private String lastName;

    @Column(name = "experience")
    private int experience;

    @Column(name = "main_subject_id")
    @ForeignKey(tableRef = "Subjects", keyRef = "id")
    private long mainSubjectId;

    public Teacher() {
    }

    public Teacher(long id, String firstName, String lastName, int experience, long mainSubjectId) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.experience = experience;
        this.mainSubjectId = mainSubjectId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public long getMainSubjectId() {
        return mainSubjectId;
    }

    public void setMainSubjectId(long mainSubjectId) {
        this.mainSubjectId = mainSubjectId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Teacher)) return false;
        Teacher teacher = (Teacher) o;
        return getId() == teacher.getId() &&
                getExperience() == teacher.getExperience() &&
                getMainSubjectId() == teacher.getMainSubjectId() &&
                getFirstName().equals(teacher.getFirstName()) &&
                getLastName().equals(teacher.getLastName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getFirstName(), getLastName(), getExperience(), getMainSubjectId());
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", experience=" + experience +
                ", mainSubjectId=" + mainSubjectId +
                '}';
    }
}
