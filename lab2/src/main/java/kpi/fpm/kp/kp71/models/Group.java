package kpi.fpm.kp.kp71.models;

import kpi.fpm.kp.kp71.annotations.Column;
import kpi.fpm.kp.kp71.annotations.Entity;
import kpi.fpm.kp.kp71.annotations.PrimaryKey;

import java.util.Date;
import java.util.Objects;

@Entity(table = "Groups")
public class Group {
    @Column(name = "id")
    @PrimaryKey
    private long id;

    @Column(name = "code")
    private String code;

    @Column(name = "entrance_year")
    private Date entranceYear;

    public Group() {
    }

    public Group(long id, String code, Date entranceYear) {
        this.id = id;
        this.code = code;
        this.entranceYear = entranceYear;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getEntranceYear() {
        return entranceYear;
    }

    public void setEntranceYear(Date entranceYear) {
        this.entranceYear = entranceYear;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Group)) return false;
        Group group = (Group) o;
        return getId() == group.getId() &&
                getCode().equals(group.getCode()) &&
                getEntranceYear().equals(group.getEntranceYear());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getCode(), getEntranceYear());
    }

    @Override
    public String toString() {
        return "Group{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", entranceYear=" + entranceYear +
                '}';
    }
}
