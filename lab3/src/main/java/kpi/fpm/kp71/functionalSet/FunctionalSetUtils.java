package kpi.fpm.kp71.functionalSet;

import java.util.function.Function;

public class FunctionalSetUtils {

    public static <T> PurelyFunctionalSet<T> empty() {
        return x -> false;
    }

    public static <T> PurelyFunctionalSet<T> singletonSet(T el) {
        return x -> x.equals(el);
    }

    public static <T> PurelyFunctionalSet<T> union(PurelyFunctionalSet<T> set1, PurelyFunctionalSet<T> set2) {
        return x -> set1.contains(x) || set2.contains(x);
    }

    public static <T> PurelyFunctionalSet<T> intersect(PurelyFunctionalSet<T> set1, PurelyFunctionalSet<T> set2) {
        return x -> set1.contains(x) && set2.contains(x);
    }

    public static <T> PurelyFunctionalSet<T> diff(PurelyFunctionalSet<T> set1, PurelyFunctionalSet<T> set2) {
        return x -> set1.contains(x) && !set2.contains(x);
    }

    public static <T> PurelyFunctionalSet<T> filter(PurelyFunctionalSet<T> set, Predicate<T> predicate) {
        return x -> set.contains(x) && predicate.invoke(x);
    }

    public static <T> boolean forAll(PurelyFunctionalSet<T> s, Predicate<Integer> p) {
        PurelyFunctionalSet<Integer> set = (PurelyFunctionalSet<Integer>) s;
        return forAllWithCounter(set, p, -1000, 1000);
    }

    public static <T> boolean exists(PurelyFunctionalSet<T> s, Predicate<Integer> p) {
        PurelyFunctionalSet<Integer> set = (PurelyFunctionalSet<Integer>) s;
        return existWithCounter(set, p, -1000, 1000);
    }

    public static <T, R> PurelyFunctionalSet<R> map(PurelyFunctionalSet<T> s, Function<Integer, R> p) {
        PurelyFunctionalSet<Integer> set = (PurelyFunctionalSet<Integer>) s;
        return mapWithCounter(set, p, -1000, 1000);
    }

    private static boolean forAllWithCounter(PurelyFunctionalSet<Integer> s, Predicate<Integer> p, Integer current, Integer maxBound) {
        if (current > maxBound)
            return true;

        if (s.contains(current) && !p.invoke(current))
            return false;

        return forAllWithCounter(s, p, current + 1, maxBound);
    }

    private static boolean existWithCounter(PurelyFunctionalSet<Integer> s, Predicate<Integer> p, Integer current, Integer maxBound) {
        if (current > maxBound)
            return false;

        if (s.contains(current) && p.invoke(current))
            return true;

        return existWithCounter(s, p, current + 1, maxBound);
    }

    private static <R> PurelyFunctionalSet<R> mapWithCounter(PurelyFunctionalSet<Integer> s, Function<Integer, R> func, Integer current, Integer maxBound) {
        if (current > maxBound)
            return empty();

        if (s.contains(current)) {
            PurelyFunctionalSet<R> set = singletonSet(func.apply(current));
            return union(set, mapWithCounter(s, func, current + 1, maxBound));
        }

        return mapWithCounter(s, func, current + 1, maxBound);
    }
}
