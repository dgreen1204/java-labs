package kpi.fpm.kp71.fs.tests;

import kpi.fpm.kp71.fs.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Binary file")
public class BinaryFileTest {
    @Test
    @DisplayName("File Creation")
    void createFileTest() {
        assertThrows(IllegalArgumentException.class,
                () -> BinaryFile.create("test1", null, "data".getBytes()));

        Directory dir1 = Directory.create("dir1", null);
        assertThrows(IllegalArgumentException.class, () -> BinaryFile.create("", dir1, "data".getBytes()));
        assertThrows(IllegalArgumentException.class, () -> BinaryFile.create("name", dir1, null));

        BinaryFile file = BinaryFile.create("file", dir1, "my data".getBytes());
        assertNotNull(file);
        assertEquals(FSNode.FSNodeType.BINARY_FILE, file.getType());
        assertEquals("file", file.getName());
        assertFalse(file.isDirectory());
    }

    @Test
    @DisplayName("Read bytes")
    void readBytesTest() {
        Directory dir = Directory.create("dir2", null);
        BinaryFile file = BinaryFile.create("file", dir, "my data".getBytes());
        assertArrayEquals("my data".getBytes(), file.read());

        BinaryFile emptyFile = BinaryFile.create("empty", dir, "".getBytes());
        assertEquals(0, emptyFile.read().length);
        assertArrayEquals( new byte[0], emptyFile.read());
    }
}

