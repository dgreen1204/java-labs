package kpi.fpm.kp.kp71.workers;

import kpi.fpm.kp.kp71.annotations.Column;
import kpi.fpm.kp.kp71.annotations.Entity;
import kpi.fpm.kp.kp71.annotations.EntityInherited;
import kpi.fpm.kp.kp71.annotations.PrimaryKey;
import org.javatuples.Pair;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class AnnotationWorker {
    public static void isEntity(Class<?> tclass) throws Exception {
        if (!tclass.isAnnotationPresent(Entity.class)) {
            throw new Exception(String.format("Class %s does not represent a table", tclass.getName()));
        }
    }

    public static String getTableName(Class<?> tclass) throws Exception {
        isEntity(tclass);
        return tclass.getAnnotation(Entity.class).table();
    }

    public static boolean isEntityInherited(Class<?> tclass) throws Exception {
        isEntity(tclass);
        if (!tclass.isAnnotationPresent(EntityInherited.class)) {
            return false;
        }
        Class baseEntity = tclass.getSuperclass();
        if (!baseEntity.isAnnotationPresent(Entity.class)) {
            throw new Exception(String.format("Base class %s does not represent a table for inherited %s",
                    baseEntity.getName(), tclass.getName()));
        }
        return true;
    }

    public static String getColumnName(Field field) {
        Column annotation = field.getAnnotation(Column.class);
        if (annotation.name().isEmpty()) {
            return field.getName();
        } else {
            return annotation.name();
        }
    }

    public static Pair<Field, Field[]> getAllColumns(Class<?> tclass) throws Exception {
        Field[] fields = tclass.getDeclaredFields();
        List<Field> listColumns = new ArrayList<>();
        Field primary = null;
        for (Field field : fields) {
            if (field.isAnnotationPresent(Column.class)) {
                if (field.isAnnotationPresent(PrimaryKey.class)) {
                    if (primary != null) {
                        System.err.println("Expected one primary key, but got more:");
                        System.err.printf("First was: %s, second: %s in class: %s%n",
                                primary.getName(), field.getName(), tclass.getName());
                        throw new Exception("Too many primary fields");
                    }
                    primary = field;
                } else {
                    listColumns.add(field);
                }
            }
        }
        if (primary == null && !isEntityInherited(tclass))
            throw new Exception(String.format("No primary field found in %s", tclass.getName()));
        else if (primary != null && isEntityInherited(tclass))
            throw new Exception(String.format("Inherited class %s shouldn't have primary key", tclass.getName()));
        return new Pair<>(primary, listColumns.toArray(new Field[0]));
    }

    public static String[] getAllColumnNames(Field[] columns) {
        String[] columnNames = new String[columns.length];
        for (int i = 0; i < columns.length; i++) {
            columnNames[i] = getColumnName(columns[i]);
        }
        return columnNames;
    }
}
