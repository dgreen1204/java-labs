package kpi.fpm.kp71.fs.tests;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import kpi.fpm.kp71.fs.*;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Log text file")
public class LogTextFileTest {
    @Test
    @DisplayName("Create file")
    void createFileTest() {
        assertThrows(IllegalArgumentException.class,
                () -> LogTextFile.create("test1", null, "data"));

        Directory dir = Directory.create("some", null);
        assertThrows(IllegalArgumentException.class,
                () ->LogTextFile.create("name/name", dir, null));

        LogTextFile file = LogTextFile.create("file sample", dir, "line");
        assertNotNull(file);
        assertEquals(FSNode.FSNodeType.LOG_TEXT_FILE, file.getType());
        assertEquals("file sample", file.getName());
        assertFalse(file.isDirectory());
    }

    @Test
    @DisplayName("Read append")
    void readAppendTest() {
        Directory dir = Directory.create("dir", null);
        LogTextFile file = LogTextFile.create("file", dir, "data some");

        assertEquals("data some", file.read());
        file.append("\nanother data");
        assertEquals("data some\nanother data", file.read());
        file.append("");
        assertEquals("data some\nanother data", file.read());
    }
}
