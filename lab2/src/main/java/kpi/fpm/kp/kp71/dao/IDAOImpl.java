package kpi.fpm.kp.kp71.dao;

import java.util.List;

public interface IDAOImpl<T> {
    // Get Object by ID
    T getEntity(long id);
    // Get List of all the objects of specified type.
    List<T> getEntityList();
}
