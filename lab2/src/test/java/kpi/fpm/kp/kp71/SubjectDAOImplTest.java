package kpi.fpm.kp.kp71;

import kpi.fpm.kp.kp71.dao.DAOImpl;
import kpi.fpm.kp.kp71.models.Subject;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@DisplayName("Subject")
@ExtendWith(MockitoExtension.class)
class SubjectDAOImplTest extends DAOImplTest {
    @Test
    @DisplayName("getEntityById")
    void testGetSubjectById() throws SQLException {
        DAOImpl<Subject> subjectDAO = new DAOImpl<>(Subject.class, mockConnection);
        when(mockResultSet.next()).thenReturn(true, false);
        Subject subjectId1 = new Subject(1, "OP");
        when(mockResultSet.getObject(anyString())).thenAnswer(new Answer() {
            public Object answer(InvocationOnMock invocation) {
                switch ((String) invocation.getArguments()[0]) {
                    case "id":          return subjectId1.getId();
                    case "title":       return subjectId1.getTitle();
                }
                return null;
            }
        });
        assertEquals(subjectId1, subjectDAO.getEntity(1));
        assertNull(subjectDAO.getEntity(15));
    }

    @Test
    @DisplayName("getEntityList")
    void testGetSubjectList() throws SQLException {
        DAOImpl<Subject> subjectDAO = new DAOImpl<>(Subject.class, mockConnection);
        when(mockResultSet.next()).thenReturn(false,true, true, true, false);
        assertEquals(subjectDAO.getEntityList().size(), 0);
        long[] ids = new long[] {1L, 2L, 3L};
        String[] titles = new String[] { "OP", "Matan", "ASD"};
        when(mockResultSet.getObject(anyString())).thenAnswer(new Answer() {
            private int countIds, countTitles = 0;
            public Object answer(InvocationOnMock invocation) {
                switch ((String) invocation.getArguments()[0]) {
                    case "id":           return ids[countIds++];
                    case "title":        return titles[countTitles++];
                }
                return null;
            }
        });
        List<Subject> list = subjectDAO.getEntityList();
        List<Subject> expectedList = new ArrayList<Subject>() {{
            add(new Subject(ids[0], titles[0]));
            add(new Subject(ids[1], titles[1]));
            add(new Subject(ids[2], titles[2]));
        }};
        assertEquals(list.size(), expectedList.size());
        for (int i = 0; i < list.size(); i++) {
            assertEquals(list.get(i), expectedList.get(i));
        }
    }
}