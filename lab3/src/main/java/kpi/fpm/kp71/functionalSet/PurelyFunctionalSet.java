package kpi.fpm.kp71.functionalSet;

public interface PurelyFunctionalSet<T> {
    boolean contains(T element);
}
