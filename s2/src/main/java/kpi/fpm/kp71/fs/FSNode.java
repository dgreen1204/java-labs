package kpi.fpm.kp71.fs;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Supplier;

public abstract class FSNode {
    public enum FSNodeType {
        DIRECTORY,
        BINARY_FILE,
        LOG_TEXT_FILE,
        BUFFER_FILE,
        TEXT_FILE
    }

    private Directory parent;
    private String name;
    protected FSNodeType fileType;
    ReadWriteLock readWriteLock = new ReentrantReadWriteLock();

    protected FSNode(String name) {
        validateName(name);
        this.name = name;
        this.parent = null;
    }

    protected FSNode(String name, Directory parent) {
        validateName(name);
        checkNull(parent, "Parent directory could not be null");
        if (parent.containsChild(name)) {
            throw new IllegalArgumentException("Element with same name already exists");
        }
        this.name = name;
        this.parent = parent;
        this.parent.appendChild(this);
    }

    protected void readonly(Runnable func) {
        readWriteLock.readLock().lock();
        func.run();
        readWriteLock.readLock().unlock();
    }

    protected void readwrite(Runnable func) {
        readWriteLock.writeLock().lock();
        func.run();
        readWriteLock.writeLock().unlock();
    }

    protected <T> T readwrite(Supplier<T> func) {
        readWriteLock.writeLock().lock();
        T ret = func.get();
        readWriteLock.writeLock().unlock();
        return ret;
    }

    protected <T> T readonly(Supplier<T> func) {
        readWriteLock.readLock().lock();
        T ret = func.get();
        readWriteLock.readLock().unlock();
        return ret;
    }

    protected void setName(String name) {
        readwrite(() -> {
            validateName(name);
            String oldName = this.name;
            this.name = name;
            if (parent != null) {
                parent.updateChild(this, oldName);
            }
        });
    }

    public String getName() {
        return readonly(() -> name);
    }

    protected Directory getParent() {
        return readonly(() -> parent);
    }

    protected Directory getRoot() {
        return readonly(() -> {
            if (parent == null) {
                return (Directory) this;
            }
            Directory current = parent;
            while (current.getParent() != null) {
                current = current.getParent();
            }
            return current;
        });
    }

    public void delete() {
        readwrite(() -> {
            if (parent != null) {
                parent.removeChild(name);
            }
            parent = null;
        });
    }

    public void move(Directory destination) {
        readwrite(() -> {
            checkNull(destination, "Destination could not be null");
            if (parent != null) {
                parent.removeChild(name);
            }
            parent = destination;
            parent.appendChild(this);
        });
    }

    public boolean isDirectory() {
        return fileType == FSNodeType.DIRECTORY;
    }

    public FSNodeType getType() {
        return this.fileType;
    }

    private static void validateName(String name) {
        if (name == null || name.length() == 0) {
            throw new IllegalArgumentException("Name could not be null or empty string");
        }
    }

    protected static void checkNull(Object node, String message) {
        if (node == null) {
            throw new IllegalArgumentException(message);
        }
    }
}
