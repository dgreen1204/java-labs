package kpi.fpm.kp71.threads;

import kpi.fpm.kp71.fs.*;
import java.util.concurrent.CyclicBarrier;

public class Thread2 extends ThreadTask {
    public Thread2(Directory root) {
        super(root);
    }

    @Override
    public void run() {
        try {
            task_init();
            barrier.await();
            task();
            countDownLatch_2.await();
            task_finalize();
            countDownLatch_4.countDown();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void task_init() throws InterruptedException {
        System.out.println("Thread 2 init");
        Thread.sleep(1000);
        LogTextFile.create("LogTextFile1", root, "data");
    }

    public void task() {
        System.out.println("Thread 2 task");
    }

    public void task_finalize() {
        System.out.println("Thread 2 finalize");
    }
}