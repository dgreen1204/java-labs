package kpi.fpm.kp.kp71;

import kpi.fpm.kp.kp71.dao.DAOImpl;
import kpi.fpm.kp.kp71.models.Mark;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@DisplayName("Mark")
@ExtendWith(MockitoExtension.class)
class MarkDAOImplTest extends DAOImplTest {
    @Test
    @DisplayName("getEntityById")
    void testGetMarkById() throws SQLException, ParseException {
        DAOImpl<Mark> markDAO = new DAOImpl<>(Mark.class, mockConnection);
        when(mockResultSet.next()).thenReturn(true, false);
        SimpleDateFormat p = new SimpleDateFormat("dd.MM.yyyy");
        Date dateMark1 = new Date(p.parse("25.09.2019").getTime());
        Mark markId1 = new Mark(1, 100, dateMark1, 1, 1, 1);
        when(mockResultSet.getObject(anyString())).thenAnswer(new Answer() {
            public Object answer(InvocationOnMock invocation) {
                switch ((String) invocation.getArguments()[0]) {
                    case "id":              return markId1.getId();
                    case "mark":            return markId1.getMark();
                    case "date":            return markId1.getDate();
                    case "subjectId":       return markId1.getSubjectId();
                    case "studentId":       return markId1.getStudentId();
                    case "teacherId":       return markId1.getTeacherId();
                }
                return null;
            }
        });
        assertEquals(markId1, markDAO.getEntity(1));
        assertNull(markDAO.getEntity(15));
    }

    @Test
    @DisplayName("getEntityList")
    void testGetMarkList() throws SQLException, ParseException {
        DAOImpl<Mark> markDAO = new DAOImpl<>(Mark.class, mockConnection);
        when(mockResultSet.next()).thenReturn(false,true, true, true, false);
        assertEquals(markDAO.getEntityList().size(), 0);
        SimpleDateFormat p = new SimpleDateFormat("dd.MM.yyyy");
        Date dateMark1 = new Date(p.parse("21.09.2019").getTime());
        Date dateMark2 = new Date(p.parse("09.04.2019").getTime());
        Date dateMark3 = new Date(p.parse("21.10.2019").getTime());
        long[]  ids = new long[]{1L, 2L, 3L};
        int[] marks = new int[] { 100, 90, 85};
        Date[] dates =  new Date[] { dateMark1, dateMark2, dateMark3};
        long[]  studentIds = new long[]{1L, 2L, 3L};
        long[]  subjectIds = new long[]{1L, 2L, 3L};
        long[]  teacherIds = new long[]{1L, 2L, 3L};
        when(mockResultSet.getObject(anyString())).thenAnswer(new Answer() {
            private int countIds, countMarks, countDates, countStIds, countSubIds, countTeachIds = 0;
            public Object answer(InvocationOnMock invocation) {
                switch ((String) invocation.getArguments()[0]) {
                    case "id":              return ids[countIds++];
                    case "mark":            return marks[countMarks++];
                    case "date":            return dates[countDates++];
                    case "subjectId":       return studentIds[countStIds++];
                    case "studentId":       return subjectIds[countSubIds++];
                    case "teacherId":       return teacherIds[countTeachIds++];
                }
                return null;
            }
        });
        List<Mark> list = markDAO.getEntityList();
        List<Mark> expectedList = new ArrayList<Mark>() {{
            add(new Mark(ids[0], marks[0], dates[0], subjectIds[0], studentIds[0], teacherIds[0]));
            add(new Mark(ids[1], marks[1], dates[1], subjectIds[1], studentIds[1], teacherIds[1]));
            add(new Mark(ids[2], marks[2], dates[2], subjectIds[2], studentIds[2], teacherIds[2]));
        }};
        assertEquals(list.size(), expectedList.size());
        for (int i = 0; i < list.size(); i++) {
            assertEquals(list.get(i), expectedList.get(i));
        }
    }
}
